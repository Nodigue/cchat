#include "server.h"

int main(int argc, char *argv[]) {
	system("clear");

	SOCKET server;
	SOCKET clients[NB_MAX];

	int max_fd = 0;
	fd_set readfs;

	int nb = 0;

	if (init_server(&server)) {
		printf("Impossible d'initialiser le server\n");
		return 1;
	}

	while (1) {
		printf("Personne en ligne : %d\n", nb);
		
		max_fd = update_FD_SET(&readfs, &server, clients, &nb);
		select(max_fd + 1, &readfs, NULL, NULL, NULL);

		if (FD_ISSET(server.socket, &readfs)) {
			if (nb < NB_MAX) {
				accept_connection(&server, clients, &nb);
			}
		}else {
			check_message(&readfs, clients, &nb);
		}
	}
	
	return 0;
}

int init_server(SOCKET *server) {
	int opt = 1;

	server->recsize = sizeof(server->sin);
	server->socket = socket(AF_INET, SOCK_STREAM, 0);

	if (server->socket < 0) {
		printf("Erreur création socket\n");
		return 1;
	}

	printf("La socket %d ouverte en mode TCP/IP sur le port %d\n", server->socket, PORT);

	if (setsockopt(server->socket, SOL_SOCKET, SO_REUSEADDR, (char *) &opt, sizeof(opt)) < 0) {
		printf("Impossible de modifier les options de la socket\n");		
		return 1;
	}

	server->sin.sin_addr.s_addr = htonl(INADDR_ANY);
	server->sin.sin_family = AF_INET;
	server->sin.sin_port = htons(PORT);

	if (bind(server->socket, (SOCKADDR*) &server->sin, server->recsize) < 0) {
		printf("Impossible de bind la socket\n");
		return 1;
	}

	if (listen(server->socket, 5) < 0) {
		printf("Impossible de lister le port");
		return 1;
	}

	printf("Listage du port %d...\n", PORT);	
	
	return 0;
}

int update_FD_SET(fd_set *readfs, SOCKET *server, SOCKET *clients, int *nb) {
	int max_fd, i;

	max_fd = server->socket;

	FD_ZERO(readfs);
	FD_SET(server->socket, readfs);

	for (i = 0; i < *nb; i++) {
		FD_SET(clients[i].socket, readfs);
		if (clients[i].socket > max_fd) max_fd = clients[i].socket;
	}

	return max_fd;
}

void accept_connection(SOCKET *server, SOCKET *clients, int *nb) {
	char buffer[BUFF_SIZE] = "";

	clients[*nb].recsize = sizeof(clients[*nb].sin);
	clients[*nb].socket = accept(server->socket, (SOCKADDR *) &clients[*nb].sin, &clients[*nb].recsize);

	if (recv(clients[*nb].socket, clients[*nb].pseudo, 20, 0) < 0) {
		printf("Erreur reception pseudo\n");
	}

	strcat(buffer, clients[*nb].pseudo);
	strcat(buffer, " a rejoint la salle de discussion\n");
	send_message_to_others(clients, clients[*nb], *nb, buffer);

	*nb = *nb + 1;
}

void check_message(fd_set *readfs, SOCKET *clients, int *nb) {
	int recv_err;

	char message[BUFF_SIZE];
	char buffer[BUFF_SIZE] = "";

	int i, j;

	for (i = 0; i < *nb; i++) {
		if (FD_ISSET(clients[i].socket, readfs)) {
			recv_err = recv(clients[i].socket, message, BUFF_SIZE, 0);

			if (recv_err == 0) {
				close(clients[i].socket);

				strcat(buffer, clients[i].pseudo);
				strcat(buffer, " a quitté la salle de discussion\n");
				send_message_to_others(clients, clients[i], *nb, buffer);		

				for (j = i; j < *nb; j++) {
					memmove(clients + j, clients + j + 1, sizeof(clients[i]));
				}

				*nb = *nb - 1;
			}else {
				buffer[recv_err] = '\0';

				strcat(buffer, clients[i].pseudo);
				strcat(buffer, " : ");
				strcat(buffer, message);
				send_message_to_others(clients, clients[i], *nb, buffer);
			}
		}
	}
}

void send_message_to_others(SOCKET *clients, SOCKET client, int nb, char *message) {
	int i;

	for (i = 0; i < nb; i++) {
		if (clients[i].socket != client.socket) {
			if (send(clients[i].socket, message, BUFF_SIZE, 0) < 0) {
				printf("Erreur envoi message\n");
			}
		}
	}
}
