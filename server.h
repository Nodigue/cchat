#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;

struct Socket {
	int socket;
	SOCKADDR_IN sin;
	int recsize;

	char pseudo[20];
};

typedef struct Socket SOCKET;

#define PORT 8888
#define NB_MAX 5
#define BUFF_SIZE 1000

int init_server(SOCKET *server);
int update_FD_SET(fd_set *readfs, SOCKET *server, SOCKET *clients, int *nb);
void accept_connection(SOCKET *server, SOCKET *clients, int *nb);
void check_message(fd_set *readfs, SOCKET *clients, int *nb);
void send_message_to_others(SOCKET *clients, SOCKET client, int nb, char *message);

