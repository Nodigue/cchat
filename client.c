#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <pthread.h>

typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;

#define PORT 8888
#define BUFF_SIZE 1000

void envoi(SOCKET sock);
void *reception(void *sock);

int server_state;

int main(int argc, char *argv[]) {
	system("clear");

	SOCKET sock;
	SOCKADDR_IN server;

	pthread_t thread;

	char pseudo[20];

	sock = socket(AF_INET, SOCK_STREAM, 0);

	if (sock < 0) {
		printf("Erreur création socket\n");
		return 1;
	}

	server.sin_addr.s_addr = inet_addr("127.0.0.1");
	server.sin_family = AF_INET;
	server.sin_port = htons(PORT);

	if (connect(sock, (SOCKADDR *) &server, sizeof(server)) < 0) {
		printf("Erreur connection\n");
		return 1;
	}else {
		server_state = 1;
		printf("Connecté à %s:%d\n", inet_ntoa(server.sin_addr), PORT);
	}

	if (pthread_create(&thread, NULL, reception, (void *) &sock) < 0) {
		printf("Erreur création thread\n");
		return 1;
	}

	strcpy(pseudo, argv[1]);
	if (send(sock, pseudo, 20, 0) < 0) {
		printf("Erreur envoi pseudo\n");
		return 1;
	}
		
	envoi(sock);
	printf("Deconnection du serveur...\n");
	close(sock);
	pthread_exit(&thread);
	
	return 0;
}

void envoi(SOCKET sock) {
	char buffer[BUFF_SIZE];

	while (server_state) {
		fgets(buffer, BUFF_SIZE, stdin);
		
		if (send(sock, buffer, BUFF_SIZE, 0) < 0) {
			printf("Erreur envoi message\n");
		}
	}
}

void *reception(void *sock) {
	SOCKET servsock = *(SOCKET*) sock;

	char buffer[BUFF_SIZE];
	int recv_err;

	while (server_state) {
		recv_err = recv(servsock, buffer, BUFF_SIZE, 0);
	
		if (recv_err < 0) {
			printf("Erreur réception message\n");
		}else if (recv_err == 0) {
			server_state = 0;
		}else {
			buffer[recv_err] = '\0';
			printf("%s", buffer);
		}
	}
}
